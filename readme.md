# Algoritmo de KMeans

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018<br />

### Objetivo
A aplicação é uma implementação do algoritmo de kmeans desenvolvida por min mesmo.
O algoritmo cálcula a distancia euclidiana para todos os elementos da entrada
e então compara com as sementes para decidir com qual dos grupos esse elemento
se assemelha mais. A partir daí uma nova semente é escolhida entre os elementos 
de cada grupo (utilizando a média) e então todo o processo é refeito a partir 
dessa nova semente. O loop continua até a semente não variar mais.

O algoritmo foi implementado utilizado 'std::list<long long int>' devido arrays
não suportarem a quantidade de elementos. Para navegar nos elementos de uma 'list'
é necessario utilizar iterador porém não consegui paralelizar loops desse tipo e 
por isso o speedup foi baixo.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [C++](www.cplusplus.com/)<br />
Banco de dados: Não utiliza<br />

### Execução
#### Sequencial

    $ g++ Kmeans.cpp -o kmeans
    $ ./kmeans
    
#### Paralela

    $ g++ kmeans.cpp -o kmeans -fopenmp
    $ ./kmeans

### Contribuição

Esse projeto está finalizado e é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->