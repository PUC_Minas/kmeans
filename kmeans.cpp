/* Tempo sequêncial     = 0m51.845s
 * Tempo para 1 thread  = 0m6.214s
 * Tempo para 2 threads = 0m3.902s
 * Tempo para 4 threads = 0m4.193s
 * Tempo para 8 threads = 0m4.580s
 * 
 * Para paralelizar não foi necessario alterar o código,
 * porém não consegui aplicar reduction em um loop que
 * utiliza iterador. Para paralelizar o loop principal
 * o 'do while' foi aplicado um 'parallel' com 'single'
 * e por dentro um 'task' pois a primeira parte so pode 
 * ser executada pela thread master e a senguda parte ja
 * pode ser dividida, porém por se tratar de uma 'list'
 * as inserções de elementos foram utilizadas 'critical'
 * para que não houvesse conflito de inserir por cima.
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <list>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <omp.h>

using namespace std;

// Input length
const long long int N = 5000000;

void showlist(list<long long int> g) {
    list<long long int>::iterator it;
    for (it = g.begin(); it != g.end(); ++it)
        cout << '\t' << *it;
    cout << '\n';
}

bool checkIfExists() {
    ifstream f("input.in");
    return f.good();
}

int main() {
    long long int t1, t2;

    std::list<long long int> input;
    std::list<long long int> cluster_1;
    std::list<long long int> cluster_2;

    std::list<long long int>::iterator input_it, cluster_1_it, cluster_2_it;

    // Generate input 
    if (!checkIfExists()) {
        int i = 0;
        for (i = 0; i < N; i++) {
            int num = rand() % N;
            input.push_back(num);
        }
        ofstream file;
        file.open("input.in");
        for (input_it = input.begin(); input_it != input.end(); input_it++) {
            file << *input_it << "\n";
        }
        file.close();
    } else {
        ifstream file;
        file.open("input.in");
        int line;
        while (file >> line) {
            input.push_back(line);
        }
        file.close();
    }

    // Initial means
    long long int seed_1 = input.front();
    long long int seed_2 = input.back();

    // Old means
    long long int mean_1, mean_2;
    #pragma omp parallel num_threads(2)
    {
        #pragma omp single private(t1, t2, mean_1, mean_2, cluster_1_it, cluster_2_it, input_it)
        {
           do {
                cluster_1.clear();
                cluster_2.clear();

                // Saving old means
                mean_1 = seed_1;
                mean_2 = seed_2;
                
                #pragma omp task
                {
                    // Creating clusters
                    for (input_it = input.begin(); input_it != input.end(); input_it++) {
                        // Calculating distance to means
                        t1 = ((*input_it - seed_1) < 0) ? ((*input_it - seed_1) * -1) : (*input_it - seed_1);
                        t2 = ((*input_it - seed_2) < 0) ? ((*input_it - seed_2) * -1) : (*input_it - seed_2);
                        if (t1 < t2) {
                            // Near to first mean
                            #pragma omp critical
                            cluster_1.push_back(*input_it);
                        } else {
                            // Near to second mean
                            #pragma omp critical
                            cluster_2.push_back(*input_it);
                        }
                    }
                    // Calculating new means
                    t2 = 0;
                    for (cluster_1_it = cluster_1.begin(); cluster_1_it != cluster_1.end(); cluster_1_it++) {
                        t2 = t2 + *cluster_1_it;
                    }
                    
                    #pragma omp critical
                    seed_1 = t2 / cluster_1.size();

                    // #pragma omp section
                    t2 = 0;
                    for (cluster_2_it = cluster_2.begin(); cluster_2_it != cluster_2.end(); cluster_2_it++) {
                        t2 = t2 + *cluster_2_it;
                    }

                    #pragma omp critical
                    seed_2 = t2 / cluster_2.size();
                }
            } while (seed_1 != mean_1 && seed_2 != mean_2);
        }
    }

    // std::cout << "\nCLUSTER 1 CONTAINS " << cluster_1.size() << " ELEMENTS";
    // std::cout << "\nCLUSTER 2 CONTAINS " << cluster_2.size() << " ELEMENTS";
    // std::cout << "\nLAST SEED OF CLUSTER 1: " << seed_1;
    // std::cout << "\nLAST SEED OF CLUSTER 2: " << seed_2;
    std::cout << "\nClusters created";
}